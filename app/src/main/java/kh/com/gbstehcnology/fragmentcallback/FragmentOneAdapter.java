package kh.com.gbstehcnology.fragmentcallback;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by GBSTech on 3/29/17.
 */

public class FragmentOneAdapter extends RecyclerView.Adapter<FragmentOneAdapter.FragmentOneViewHolder> {

    private ArrayList<ListItem> listItems;
    private Context context;
    CustomCallBack customCallBack;

    public FragmentOneAdapter(ArrayList<ListItem> listItems, CustomCallBack customCallBack){
        this.listItems = listItems;
        this.customCallBack = customCallBack;
    }
    @Override
    public FragmentOneViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_one_item, parent,false);
        FragmentOneViewHolder holder = new FragmentOneViewHolder(view);

        return holder;
    }
    @Override
    public void onBindViewHolder(FragmentOneViewHolder holder, int position) {
        holder.tvName.setText(listItems.get(position).getName());
        holder.ivProfile.setImageResource(listItems.get(position).getProfile());
    }
    @Override
    public int getItemCount() {
        return listItems.size();
    }
    class FragmentOneViewHolder extends RecyclerView.ViewHolder{
        TextView tvName;
        ImageView ivProfile;
        LinearLayout llFooter;

        public FragmentOneViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            ivProfile = (ImageView) itemView.findViewById(R.id.iv_profile);
            llFooter= (LinearLayout) itemView.findViewById(R.id.ll_footer);

            ivProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    customCallBack.onProfileClick(getAdapterPosition());
                }
            });
        }
    }

//    MY Custom CallBack
    interface CustomCallBack{
        void onProfileClick(int position);
    }
}
