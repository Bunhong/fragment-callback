package kh.com.gbstehcnology.fragmentcallback;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class OneFragment extends Fragment implements FragmentOneAdapter.CustomCallBack{

    private ArrayList<ListItem> listItems;
    private RecyclerView recyclerView;
    private FragmentOneAdapter adapter;

    public OneFragment() { }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_one, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(manager);
        listItems = new ArrayList<>();
        for(int i=0;i<40;i++){
            listItems.add(new ListItem(R.drawable.ic_account_circle_black_48dp, "MY NAME "+ i));
        }
        adapter = new FragmentOneAdapter(listItems, this);
//        adapter = new FragmentOneAdapter(listItems, getActivity());
//        adapter.setCustomCallBack(this);
        recyclerView.setAdapter(adapter);
        return view;
    }

    @Override
    public void onProfileClick(int position) {
        Toast.makeText(getActivity(), "position "+ position, Toast.LENGTH_SHORT).show();
    }

}
