package kh.com.gbstehcnology.fragmentcallback;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ListView lvNavigation;
    private DrawerLayout mDrawer;
    private NavigationView mNavigation;
    private LinearLayout llMianContain;
    String[] navigation = new String[]{"Nav One","Nav Two", "Nav Three","Nav Four"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        lvNavigation = (ListView) findViewById(R.id.lv_listView);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mNavigation= (NavigationView) findViewById(R.id.navigation_view);
        llMianContain = (LinearLayout) findViewById(R.id.ll_main_content);

        setSupportActionBar(toolbar);
        lvNavigation.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, navigation));
        lvNavigation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                replaceFragment(i);
                lvNavigation.setItemChecked(i, true);
                hideDrawer();
            }
        });

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.open, R.string.close);
        mDrawer.setDrawerListener(toggle);
        toggle.syncState();
    }
    private void replaceFragment(int position){
        Fragment fragment = null;
        switch (position){
            case 0:
                fragment = new OneFragment();
                break;
            case 1:
                fragment = new TwoFragment();
                break;
            case 2:
                fragment = new ThreeFragment();
                break;
            case 3:
                fragment= new FourFragment();
                break;
            default:
                fragment = new OneFragment();
                break;
        }
        if(null!= fragment){
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.ll_main_content, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }
    private void hideDrawer(){
        mDrawer.closeDrawer(GravityCompat.START);
    }
    private void showDrawer(){
        mDrawer.openDrawer(GravityCompat.START);
    }
    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(GravityCompat.START))
            hideDrawer();
        else
            super.onBackPressed();
    }
}
