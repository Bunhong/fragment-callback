package kh.com.gbstehcnology.fragmentcallback;

/**
 * Created by GBSTech on 3/29/17.
 */

public class ListItem {

    private int profile;
    private String name;

    public ListItem(){}

    public ListItem(int profile, String name) {
        this.profile = profile;
        this.name = name;
    }

    public int getProfile() {
        return profile;
    }

    public void setProfile(int profile) {
        this.profile = profile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
